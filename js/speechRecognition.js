var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
var recognition = new SpeechRecognition();


recognition.lang = 'en-US';
recognition.interimResults = false;
recognition.maxAlternatives = 1;


var input = document.getElementById('myInput');



input.onclick = function() {
    recognition.start();
    console.log('Ready to receive a command.');
}

recognition.onresult = function(event) {


    var last = event.results.length - 1;
    let command = event.results[last][0].transcript;

    input.value = command;
    console.log('Confidence: ' + event.results[0][0].confidence);
}

recognition.onspeechend = function() {
    recognition.stop();
}

recognition.onnomatch = function(event) {
    console.log("I didn't recognise that command.");
}

recognition.onerror = function(event) {
    console.log('Error occurred in recognition: ' + event.error);
}
